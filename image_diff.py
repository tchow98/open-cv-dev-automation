import cv2
import imutils
from skimage.metrics import structural_similarity as compare_ssim
from VersionEntity import VersionEntity
import file_control


def initiate_image_diff(original_version: VersionEntity, testing_version: VersionEntity):
    original_dir = original_version.getProcessedDestination()
    testing_version_dir = testing_version.getProcessedDestination()

    # load the two input images
    original_image = cv2.imread(original_dir)
    testing_version_image = cv2.imread(testing_version_dir)
    # convert the images to grayscale
    grayoriginal = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    grayversion_change = cv2.cvtColor(testing_version_image, cv2.COLOR_BGR2GRAY)

    # compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    (score, diff) = compare_ssim(grayoriginal, grayversion_change, full=True)
    diff = (diff * 255).astype("uint8")
    print('SSIM: {}'.format(score) + '\t' + testing_version.version_id)

    # threshold the difference image, followed by finding contours to
    # obtain the regions of the two input images that differ
    thresh = cv2.threshold(diff, 0, 255,
                           cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    # loop over the contours
    for c in cnts:
        # compute the bounding box of the contour and then draw the
        # bounding box on both input images to represent where the two
        # images differ
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(original_image, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.rectangle(testing_version_image, (x, y), (x + w, y + h), (0, 0, 255), 2)

    # create directories if not exist
    file_control.create_dir(file_control.getResultsDestination(original_version.getCorrespondingScreenDirectory()))

    # write to results folder
    # cv2.imwrite(original_version.getResultsDestination(), original_image)
    cv2.imwrite(testing_version.getResultsDestination(), testing_version_image)

    # cv2.imshow(original_title, original_image)
    # cv2.imshow(version_change_title, testing_version_image)
    # cv2.waitKey(0)
