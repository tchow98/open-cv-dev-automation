import preprocess_images
import image_diff
import file_control
import text_detection

SCREEN = 'home-localization'
# ALGORITHM = 'IMAGE_DIFF'
ALGORITHM = 'LOCALIZATION_TEST'

def main():
    # ONLY FOCUS ON SIGN IN SCREEN FOR NOW #
    serialized_images = file_control.serializeImages()

    versions = serialized_images[SCREEN]

    if ALGORITHM == 'IMAGE_DIFF':
        preprocess_images.initiate_processing(versions, True)
        original_version = list(filter(lambda x: x.original, versions))[0]

        # initiate SSIM comparisons
        for testing_version in versions:
            if not testing_version.original:
                image_diff.initiate_image_diff(original_version, testing_version)
    else:
        preprocess_images.initiate_processing(versions, False)
        for testing_version in versions:
            text_detection.initiate_text_detection(testing_version)





if __name__ == "__main__":
    main()