class VersionEntity:
    def __init__(self, version_id, directory, original=False):
        self.version_id = version_id
        self.directory = directory
        self.original = original

    def getCorrespondingScreenDirectory(self):
        folders = self.directory.split('/')
        str_ret = ""
        for i in range(3):
            str_ret += folders[i] + '/'
        return str_ret

    def getWorkingFolderName(self):
        folders = self.directory.split('/')
        return folders[len(folders)-2]

    def getVersionName(self):
        folders = self.directory.split('/')
        return folders[len(folders)-1].replace('.png', '')

    def getLanguage(self):
        if 'english' in self.directory:
            return 'en'
        else:
            return 'fr'

    def getProcessedDestination(self):
        return self.directory.replace('images', 'processed')

    def getResultsDestination(self):
        return self.directory.replace('images', 'results')
