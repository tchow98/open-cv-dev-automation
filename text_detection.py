# Import required packages
import cv2
import pytesseract
from VersionEntity import VersionEntity
import file_control
from googletrans import Translator
import re

def initiate_text_detection(version: VersionEntity):
    language = version.getLanguage()

    # Mention the installed location of Tesseract-OCR in your system
    pytesseract.pytesseract.tesseract_cmd = '/usr/local/Cellar/tesseract/4.1.1/bin/tesseract'

    # Read image from which text needs to be extracted
    img = cv2.imread(version.getProcessedDestination())

    # Preprocessing the image starts

    # Convert the image to gray scale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Performing OTSU threshold
    ret, thresh1 = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

    # Specify structure shape and kernel size.
    # Kernel size increases or decreases the area
    # of the rectangle to be detected.
    # A smaller value like (10, 10) will detect
    # each word instead of a sentence.
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18, 18))

    # Appplying dilation on the threshold image
    dilation = cv2.dilate(thresh1, rect_kernel, iterations=1)

    # Finding contours
    contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)

    # Creating a copy of image
    im2 = img.copy()

    # A text file is created and flushed
    recognized_text_directory = './results (recognized-text)/' + version.getWorkingFolderName() + '/'
    recognized_text_file_directory = recognized_text_directory + version.getVersionName() + '-recognized.txt'
    file_control.create_dir(recognized_text_directory)
    file = open(recognized_text_file_directory, "w+")
    file.write("")
    file.close()

    # Looping through the identified contours
    # Then rectangular part is cropped and passed on
    # to pytesseract for extracting text from it
    # Extracted text is then written into the text file
    for cnt in contours:
        (x, y, w, h) = cv2.boundingRect(cnt)
        # Drawing a rectangle on copied image
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # Cropping the text block for giving input to OCR
        cropped = im2[y:y + h, x:x + w]

        # Open the file in append mode
        file = open(recognized_text_file_directory, "a")

        # Apply OCR on the cropped image
        text = pytesseract.image_to_string(cropped)

        # Appending the text into file
        cleaned_text = preprocess_text(text)

        if cleaned_text != '' and cleaned_text != ' ':
            print(cleaned_text)
            detected_lang = find_language_score(language, cleaned_text)
            if detected_lang:
                write_text = detected_lang + '\t=========> ' + cleaned_text
            else:
                write_text = cleaned_text
            file.write(write_text)
            file.write("\n")


        # Close the file
        file.close
    # saving the drawn boxes
    cv2.imwrite(version.getResultsDestination(), img)

def find_language_score(lang, raw_text):
    translator = Translator()
    lang_detect = translator.detect(raw_text)
    print(lang_detect)

    if lang != lang_detect.lang:
        return lang_detect.lang
    else:
        return None
    print()


def preprocess_text(text):
    # remove new lines
    text = text.replace('\n', ' ')

    # remove excess spaces
    text = re.sub(r'\s+', ' ', text)

    # remove numbers
    text = re.sub(r'[^a-zA-z.,!?/:;\"\'\s]', '', text)

    # remove special characters ( non alpha)
    text = re.sub(r'[^a-zA-z0-9.,!?/:;\"\'\s]', '', text)

    # remove excess space and tabs
    text = re.sub(r'^\s*|\s\s*', ' ', text).strip()

    return text