from PIL import Image
from pathlib import Path
from VersionEntity import VersionEntity
import file_control
import main



def initiate_processing(versions, image_diff_algorithm):
    # create directories if not exist
    for test_version in versions:
        file_control.create_dir(file_control.getStartFolderOfDirectory(test_version.getProcessedDestination()))
        file_control.create_dir(file_control.getStartFolderOfDirectory(test_version.getResultsDestination()))


    # preprocess original first
    if image_diff_algorithm:
        crop_statusbar_ios(list(filter(lambda x: x.original, versions))[0])

    # initiate preprocessing
    for test_version in versions:
        if not test_version.original:
            crop_statusbar_ios(test_version)
        # resize_dimensions(original_version, test_version)

# def check_exception_case(test_version: VersionEntity):
#     im = Image.open(test_version.directory)
#     image_dimensions = im.size
#
#     # EXCEPTION RESIZING
#     if image_dimensions == (1242, 2208):
#         return true


# IMPORTANT: resizes dimensions for files in processed directory
def resize_dimensions(original_version: VersionEntity, test_version: VersionEntity):
    if not test_version.original:
        # resize test version
        image = Image.open(test_version.getProcessedDestination())

        new_image = image.resize(get_processed_resolution(original_version))

        # create directory if not exist
        file_control.create_dir(file_control.getProcessedDestination(test_version.getCorrespondingScreenDirectory()))
        print('Resized ' + file_control.getProcessedDestination(test_version.directory))
        new_image.save(test_version.getProcessedDestination())

def get_resolution(version: VersionEntity):
    return Image.open(version.directory).size

def get_processed_resolution(version: VersionEntity):
    return Image.open(version.getProcessedDestination()).size

def crop_statusbar_ios(version_entity: VersionEntity):
    im = Image.open(version_entity.directory)
    # 2436
    image_dimensions = im.size

    # # EXCEPTION RESIZING
    # if image_dimensions == (1242, 2208):
    #     im = im.resize((1080, 1920))

    im = im.crop(get_crop_tuple(image_dimensions[0], image_dimensions[1], main.ALGORITHM == 'LOCALIZATION_TEST'))


    # save in directory. NOTE: if original, then save to results as well
    if(version_entity.original):
        im.save(file_control.getResultsDestination(version_entity.directory), quality=95)
    im.save(file_control.getProcessedDestination(version_entity.directory), quality=95)

    print('Cropped ' + file_control.getProcessedDestination(version_entity.directory))


def get_crop_tuple(width, height, top_only):
    resolutions_data = file_control.read_json('resolutions.json')
    for resolution in resolutions_data['ios_resolutions']:
        if width == resolution['width'] and height == resolution['height']:
            if top_only:
                return resolution['cropTopOnly']
            return resolution['crop']
    print('DID NOT CROP. UNKNOWN RESOLUTION: ' + str(width) + 'x' + str(height))
    return (0, 0, width, height)


