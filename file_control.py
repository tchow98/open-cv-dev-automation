import os
import glob
import VersionEntity as VE
import json

def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def getEndFolderOfDirectory(directory):
    folders = directory.split('/')
    return folders[len(folders)-1]

def getStartFolderOfDirectory(directory):
    folders = directory.split('/')

    # build folder directory
    ret_dir = ''
    for directory_name in folders:
        if 'png' not in directory_name:
            ret_dir = ret_dir + directory_name + '/'

    return ret_dir

def getProcessedDestination(current_dir):
    return current_dir.replace('images', 'processed')

def getResultsDestination(current_dir):
    return current_dir.replace('images', 'results')

def serializeImages():
    screens = glob.glob('./images/*')
    screen_dict = {}
    for screen_directory in screens:
        # initialize key for dict
        screen_name = getEndFolderOfDirectory(screen_directory)
        screen_dict[screen_name] = []
        versions = glob.glob(screen_directory + '/*.png')
        for version_directory in versions:
            version_id = getEndFolderOfDirectory(version_directory).replace('.png', '')
            original = 'original' in version_directory
            screen_dict[screen_name].append(VE.VersionEntity(version_id, version_directory, original))
    return screen_dict

def read_json(file_name):
    with open(file_name) as f:
        data = json.load(f)

    return data